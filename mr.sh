#! /usr/bin/env bash

# This Bash script operates in a Git repository for creating a merge
# request on GitLab. It verifies if a GITLAB_TOKEN environment variable
# is set, obtains details about the current branch and project from the
# user's working directory, constructs a URL-encoded namespace for the
# GitLab project, sets up parameters for the new merge request, sends a
# POST request to create the merge request via the GitLab API, checks if
# the merge request was created successfully, extracts the web URL of
# the new merge request from the response and prints it out. If
# unsuccessful, it provides an error message along with the response
# received from GitLab.

# Check if a GITLAB_TOKEN is set
if [ -z "$GITLAB_TOKEN" ]; then
    echo "GITLAB_TOKEN is not set. Please set your GitLab API token in" \
         "the GITLAB_TOKEN environment variable."
    exit 1
fi

# Get the current branch name
current_branch=$(git rev-parse --abbrev-ref HEAD)

# Get the name of the current user
username=$USER

# Get the name of the current working directory
project_name="$(basename "$PWD")"

# Construct the project namespace and encode the slash
project_namespace="${username}/${project_name}"
url_encoded_namespace=$(echo "$project_namespace" | sed 's:/:%2F:g')

# Base URL of your GitLab instance
gitlab_url="https://gitlab.com"

# Title for the merge request
mr_title="Merge Request for $current_branch"

# Description for the merge request
mr_description="Auto-generated merge request from $current_branch branch"

# Target branch to merge into
target_branch="main"

url="${gitlab_url}/api/v4/projects/${url_encoded_namespace}/merge_requests"

# Create the merge request
response=$(curl --silent --request POST "${url}" \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --form "source_branch=${current_branch}" \
    --form "target_branch=${target_branch}" \
    --form "title=${mr_title}" \
    --form "description=${mr_description}")

# Check if the merge request was created successfully
echo $response | grep -q "iid"
if [ $? -eq 0 ]; then
    # Use jq to parse the response and get the web URL
    web_url=$(echo "$response" | jq -r '.web_url')
    echo "local:"
    echo "local: Automatically created merge request:"
    echo "local:   ${web_url}"
    echo "local:"
else
    echo "Failed to create merge request."
    echo "Response from GitLab: $response"
    exit 1
fi

